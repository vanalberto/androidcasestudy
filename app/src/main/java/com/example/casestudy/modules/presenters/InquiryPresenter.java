package com.example.casestudy.modules.presenters;

import android.util.Patterns;
import android.view.View;

import com.example.casestudy.modules.models.IInquiryModel;
import com.example.casestudy.modules.objects.Inquiry;
import com.example.casestudy.modules.views.IInquiryView;

import java.util.regex.Pattern;

public class InquiryPresenter implements IInquiryPresenter, IInquiryModel {
    private IInquiryView iInquiryView;

    public InquiryPresenter(IInquiryView iInquiryView){
        this.iInquiryView = iInquiryView;
    }


    @Override
    public void onSendInquiry(Inquiry inquiry) {

        if(inquiry.getFullName().equals("")){
            iInquiryView.onFailedInquiry("invalidName");
            return;
        }

        if(inquiry.getEmailAddress().equals("")){
            iInquiryView.onFailedInquiry("invalidEmail");
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(inquiry.getEmailAddress()).matches()){
            iInquiryView.onFailedInquiry("invalidEmail");
            return;
        }

    }

    @Override
    public void onSuccess(String message) {

    }

    @Override
    public void onFailure(String message) {

    }
}
