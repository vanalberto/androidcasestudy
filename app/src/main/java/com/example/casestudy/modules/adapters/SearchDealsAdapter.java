package com.example.casestudy.modules.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.casestudy.R;
import com.example.casestudy.modules.objects.Deal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SearchDealsAdapter extends RecyclerView.Adapter<SearchDealsAdapter.ViewHolder>{

    private ArrayList<Deal> mData;


    public SearchDealsAdapter( ArrayList<Deal> data) {
        this.mData = data;
    }

    public SearchDealsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_deals_recyclerview_cell, viewGroup, false);

        return new SearchDealsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchDealsAdapter.ViewHolder holder, int position) {
        String dealTitle = mData.get(position).getDealTitle();
        String dealCompany = mData.get(position).getDealCompany();

        Picasso.get().load(mData.get(position).getImage()).fit().centerCrop().into(holder.imageView);

        holder.textView1.setText(dealTitle);
        holder.textView2.setText(dealCompany);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void filterByCategory(ArrayList<String> categories, ArrayList<Deal> deals){
        ArrayList<Deal> filteredDeals = new ArrayList<>();

        for(Deal deal : deals){
            if(categories.contains(deal.getDealCategory())){
                filteredDeals.add(deal);
            }
        }

        this.mData.clear();
        this.mData.addAll(filteredDeals);
        notifyDataSetChanged();
    }

    public void sortDealsAlphabeticallyAscending(){
        Collections.sort(this.mData, new Comparator<Deal>() {
            @Override
            public int compare(Deal d1, Deal d2) {
                return d1.getDealCompany().compareTo(d2.getDealCompany());
            }
        });

        notifyDataSetChanged();
    }

    public void sortDealsAlphabeticallyDescending(){
        Collections.sort(this.mData, new Comparator<Deal>() {
            @Override
            public int compare(Deal d1, Deal d2) {
                return d2.getDealCompany().compareTo(d1.getDealCompany());
            }
        });

        notifyDataSetChanged();
    }

    public void sortDealsMostRecent(){
        Collections.sort(this.mData, new Comparator<Deal>() {
            @Override
            public int compare(Deal d1, Deal d2) {
                return d2.getDealCreatedOn().compareTo(d1.getDealCreatedOn());
            }
        });

        notifyDataSetChanged();
    }

    public void filterSearch(String text, ArrayList<Deal> deals){
        this.mData.clear();

        if(text.isEmpty()){
            mData.addAll(deals);
        }
        else{
            text = text.toLowerCase();
            for(Deal deal: deals){
                if(deal.getDealCompany().toLowerCase().contains(text) || deal.getDealCompany().toLowerCase().contains(text)){
                    this.mData.add(deal);
                }
            }
        }

        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView1;
        TextView textView2;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.dealResultTitleTextView);
            textView2 = itemView.findViewById(R.id.dealResultCompanyTextView);
            imageView = itemView.findViewById(R.id.dealResultImg);
            /*textView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onTextClick(view, getAdapterPosition());
                    }
                }
            });

            imageView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onImageClick(view, getAdapterPosition());
                    }
                }
            });*/

        }
    }

}
