package com.example.casestudy.modules.presenters;

import com.example.casestudy.modules.objects.Inquiry;

public interface IInquiryPresenter {
    void onSendInquiry(Inquiry inquiry);
}
