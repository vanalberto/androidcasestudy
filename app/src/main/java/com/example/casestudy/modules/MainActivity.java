package com.example.casestudy.modules;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.casestudy.R;
import com.example.casestudy.modules.fragments.HomeFragment;
import com.example.casestudy.modules.fragments.HomeTopBarFragment;
import com.example.casestudy.modules.fragments.SearchFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initActivity();

    }

    public void initActivity(){
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigation_home:
                                if(!item.isChecked()){
                                    removeAllFragments(getSupportFragmentManager());
                                    loadHomeFragments(getSupportFragmentManager());
                                }
                                break;
                            case R.id.navigation_search:
                                if(!item.isChecked()){
                                    removeAllFragments(getSupportFragmentManager());
                                    loadSearchFragment(getSupportFragmentManager());
                                }
                                break;
                        }


                        return true;
                    }
                });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        HomeTopBarFragment homeTopBarFragment = new HomeTopBarFragment();
        HomeFragment homeFragment =  new HomeFragment();

        fragmentTransaction.add(R.id.main_container, homeTopBarFragment);
        fragmentTransaction.add(R.id.main_container, homeFragment);

        fragmentTransaction.commit();
    }

    private static void removeAllFragments(FragmentManager fragmentManager) {
        for (Fragment fragment : fragmentManager.getFragments()) {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }

    private static void loadHomeFragments(FragmentManager fragmentManager){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        HomeTopBarFragment homeTopBarFragment = new HomeTopBarFragment();
        HomeFragment homeFragment =  new HomeFragment();

        fragmentTransaction.add(R.id.main_container, homeTopBarFragment);
        fragmentTransaction.add(R.id.main_container, homeFragment);

        fragmentTransaction.commit();
    }

    private static void loadSearchFragment(FragmentManager fragmentManager){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        SearchFragment searchFragment = new SearchFragment();

        fragmentTransaction.add(R.id.main_container, searchFragment);

        fragmentTransaction.commit();
    }

}
