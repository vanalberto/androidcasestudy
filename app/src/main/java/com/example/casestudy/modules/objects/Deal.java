package com.example.casestudy.modules.objects;

import java.util.Date;

public class Deal {
    private int image;

    public int getImage(){
        return image;
    }
    public void setImage(int drawableNumber) {this.image = drawableNumber; }

    private String dealTitle;

    public String getDealTitle(){ return dealTitle; }
    public void setDealTitle(String dealTitle) {this.dealTitle = dealTitle; }

    private String dealCompany;

    public String getDealCompany(){ return dealCompany; }
    public void setDealCompany(String dealCompany) {this.dealCompany = dealCompany; }

    private String dealCategory;

    public String getDealCategory(){ return this.dealCategory; }
    public void setDealCategory(String dealCategory){ this.dealCategory = dealCategory; }

    private Date dealCreatedOn;

    public Date getDealCreatedOn(){ return dealCreatedOn; }
    public void setDealCreatedOn(Date dealCreatedOn) { this.dealCreatedOn = dealCreatedOn; }

}
