package com.example.casestudy.modules.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.casestudy.R;
import com.example.casestudy.modules.adapters.CategoryAdapter;
import com.example.casestudy.modules.adapters.DealsAdapter;
import com.example.casestudy.modules.objects.Category;
import com.example.casestudy.modules.objects.Deal;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class LatestDealsFragment extends Fragment {
    private View view;

    DealsAdapter dealsAdapter;
    private ArrayList<Deal> latestDeals;

    public LatestDealsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_latest_deals, container, false);

        initLatestDeals();

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewLatestDeals);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        dealsAdapter = new DealsAdapter(latestDeals);
        recyclerView.setAdapter(dealsAdapter);

        // Inflate the layout for this fragment
        return view;
    }


    public void initLatestDeals(){
        latestDeals = new ArrayList<Deal>();

        Deal deal1 = new Deal();
        deal1.setImage(R.drawable.carousel_pic1);
        deal1.setDealTitle("10% Off Car");
        deal1.setDealCompany("HONDA EXCLUSIVE");
        latestDeals.add(deal1);

        Deal deal2 = new Deal();
        deal2.setImage(R.drawable.carousel_pic2);
        deal2.setDealTitle("10% Off Travels");
        deal2.setDealCompany("Cebu Pacific");
        latestDeals.add(deal2);


        Deal deal3 = new Deal();
        deal3.setImage(R.drawable.carousel_pic3);
        deal3.setDealTitle("10% Off Stays");
        deal3.setDealCompany("AIRBNB");
        latestDeals.add(deal3);

    }

}
