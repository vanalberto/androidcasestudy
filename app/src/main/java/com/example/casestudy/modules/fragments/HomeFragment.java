package com.example.casestudy.modules.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.casestudy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        HomeCarouselFragment homeCarouselFragment = new HomeCarouselFragment();
        CategoriesFragment categoriesFragment = new CategoriesFragment();
        SearchNearbyFragment searchNearbyFragment = new SearchNearbyFragment();
        LatestDealsFragment latestDealsFragment = new LatestDealsFragment();
        ExclusivesFragment exclusivesFragment = new ExclusivesFragment();

        fragmentTransaction.add(R.id.fragments_container, homeCarouselFragment);
        fragmentTransaction.add(R.id.fragments_container, categoriesFragment);
        fragmentTransaction.add(R.id.fragments_container, searchNearbyFragment);
        fragmentTransaction.add(R.id.fragments_container, latestDealsFragment);
        fragmentTransaction.add(R.id.fragments_container, exclusivesFragment);

        fragmentTransaction.commit();

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

}
