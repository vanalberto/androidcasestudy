package com.example.casestudy.modules.fragments;


import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.casestudy.R;
import com.example.casestudy.modules.adapters.CategoryAdapter;
import com.example.casestudy.modules.objects.Category;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {
    private View view;

    CategoryAdapter categoryAdapter;
    private ArrayList<Category> categories;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_categories, container, false);

        initCategory();

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewCategories);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        categoryAdapter = new CategoryAdapter(categories);
        recyclerView.setAdapter(categoryAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        return view;
    }

    public void initCategory(){
        categories = new ArrayList<Category>();

        Category category1 = new Category();
        category1.setImage(R.drawable.ic_car);
        category1.setCategoryName("Automotive");
        categories.add(category1);

        Category category2 = new Category();
        category2.setImage(R.drawable.ic_food);
        category2.setCategoryName("Dining");
        categories.add(category2);

        Category category3 = new Category();
        category3.setImage(R.drawable.ic_beach);
        category3.setCategoryName("Leisure");
        categories.add(category3);

        Category category4 = new Category();
        category4.setImage(R.drawable.ic_calendar);
        category4.setCategoryName("Events");
        categories.add(category4);

        Category category5 = new Category();
        category5.setImage(R.drawable.ic_shop);
        category5.setCategoryName("Shopping");
        categories.add(category5);

    }

}
