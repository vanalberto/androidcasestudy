package com.example.casestudy.modules.fragments;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.casestudy.R;
import com.example.casestudy.modules.adapters.CategoryAdapter;
import com.example.casestudy.modules.adapters.SearchCategoryAdapter;
import com.example.casestudy.modules.adapters.SearchDealsAdapter;
import com.example.casestudy.modules.models.IItemClickListener;
import com.example.casestudy.modules.objects.Category;
import com.example.casestudy.modules.objects.Deal;

import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements IItemClickListener {
    private View view;

    SearchCategoryAdapter searchCategoryAdapter;
    SearchDealsAdapter searchDealsAdapter;

    private ArrayList<Category> categories;
    private ArrayList<Deal> deals;

    private String[] sortOptions = {"Most Recent", "Ascending (A-Z)", "Descending (A-Z)"};

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);

        initCategory();
        initDeals();

        RecyclerView recyclerViewCategories = view.findViewById(R.id.recyclerViewCategoriesSearch);
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        searchCategoryAdapter = new SearchCategoryAdapter(categories);
        searchCategoryAdapter.setClickListener(this);
        recyclerViewCategories.setAdapter(searchCategoryAdapter);

        ArrayList<Deal> emptyDeal = new ArrayList<Deal>();

        RecyclerView recyclerViewSearchResults = view.findViewById(R.id.recyclerViewSearchResults);
        recyclerViewSearchResults.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        searchDealsAdapter = new SearchDealsAdapter(emptyDeal);
        recyclerViewSearchResults.setAdapter(searchDealsAdapter);

        LinearLayout sortBtn = view.findViewById(R.id.sortBtn);
        sortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSortClick();
            }
        });

        SearchView searchView = view.findViewById(R.id.SearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchDealsAdapter.filterSearch(query, deals);

                checkResults();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchDealsAdapter.filterSearch(newText, deals);

                checkResults();
                return true;
            }
        });

        checkResults();

        return view;
    }

    @Override
    public void onImageClick(View itemView, int position) {
        Log.e("sampleTag", "click card");

        ArrayList<String> selectedCategories = searchCategoryAdapter.toggleCategory(itemView, position);
        Log.e("sampleTag", selectedCategories.toString());
        searchDealsAdapter.filterByCategory(selectedCategories, deals);
        checkResults();
    }

    public void onSortClick(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("SORT BY");

        // add a list
        builder.setItems(sortOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        searchDealsAdapter.sortDealsMostRecent();
                        break;
                    case 1:
                        searchDealsAdapter.sortDealsAlphabeticallyAscending();
                        break;
                    case 2:
                        searchDealsAdapter.sortDealsAlphabeticallyDescending();
                        break;
                }
            }
        });

        // create and show alert dialog
        AlertDialog dialog = builder.create();

        checkResults();
        dialog.show();
    }

    public void checkResults(){
        if(searchDealsAdapter.getItemCount() == 0){
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


            Fragment fragment = fragmentManager.findFragmentByTag("noResults");

            if(fragment == null){
                NoResultsFragment noResultsFragment = new NoResultsFragment();

                fragmentTransaction.add(R.id.search_results_container, noResultsFragment, "noResults");

                fragmentTransaction.commit();
            }
            else {
                fragmentTransaction.show(fragment);
                fragmentTransaction.commit();
            }

            LinearLayout sortOption = view.findViewById(R.id.sort_option);
            sortOption.setVisibility(View.GONE);
        }
        else {
            FragmentManager fragmentManager = getChildFragmentManager();

            Fragment noResultsFragment = fragmentManager.findFragmentByTag("noResults");

            if(noResultsFragment != null){
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.hide(noResultsFragment);
                fragmentTransaction.commit();
            }

            LinearLayout sortOption = view.findViewById(R.id.sort_option);
            sortOption.setVisibility(View.VISIBLE);
        }
    }

    public void initCategory(){
        categories = new ArrayList<Category>();

        Category category1 = new Category();
        category1.setImage(R.drawable.ic_car_white_48);
        category1.setCategoryName("Automotive");
        categories.add(category1);

        Category category2 = new Category();
        category2.setImage(R.drawable.ic_food_white_48);
        category2.setCategoryName("Dining");
        categories.add(category2);

        Category category3 = new Category();
        category3.setImage(R.drawable.ic_beach_white_48);
        category3.setCategoryName("Leisure");
        categories.add(category3);

        Category category4 = new Category();
        category4.setImage(R.drawable.ic_calendar_white_48);
        category4.setCategoryName("Events");
        categories.add(category4);

        Category category5 = new Category();
        category5.setImage(R.drawable.ic_shop_white_48);
        category5.setCategoryName("Shopping");
        categories.add(category5);

    }

    public void initDeals(){
        deals = new ArrayList<Deal>();

        Deal deal1 = new Deal();
        deal1.setImage(R.drawable.carousel_pic1);
        deal1.setDealTitle("10% Off Car");
        deal1.setDealCompany("HONDA EXCLUSIVE");
        deal1.setDealCategory("Automotive");
        deal1.setDealCreatedOn(new Date(2019, 8, 1));
        deals.add(deal1);

        Deal deal2 = new Deal();
        deal2.setImage(R.drawable.carousel_pic2);
        deal2.setDealTitle("10% Off Travels");
        deal2.setDealCompany("Cebu Pacific");
        deal2.setDealCategory("Leisure");
        deal2.setDealCreatedOn(new Date(2019, 1, 1));
        deals.add(deal2);


        Deal deal3 = new Deal();
        deal3.setImage(R.drawable.carousel_pic3);
        deal3.setDealTitle("10% Off Stays");
        deal3.setDealCompany("AIRBNB");
        deal3.setDealCategory("Leisure");
        deal3.setDealCreatedOn(new Date(2019, 9, 1));
        deals.add(deal3);

    }

}
