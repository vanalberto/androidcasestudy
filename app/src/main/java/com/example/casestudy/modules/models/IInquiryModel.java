package com.example.casestudy.modules.models;

public interface IInquiryModel {
    public void onSuccess(String message);
    public void onFailure(String message);
}
