package com.example.casestudy.modules.models;

import android.view.View;

public interface IItemClickListener {
    //view on which user clicked and the post_id for which you want to
    //some specific action
    void onImageClick(View itemView, int post_id);
}
