package com.example.casestudy.modules.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.casestudy.R;
import com.example.casestudy.modules.objects.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{
    private List<Category> mData;

    public CategoryAdapter( ArrayList<Category> data) {
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_recyclerview_cell, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String categoryName = mData.get(position).getCategoryName();
        Picasso.get().load(mData.get(position).getImage()).into(holder.imageView);

        holder.textView.setText(categoryName);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.categoryText);
            imageView = itemView.findViewById(R.id.categoryImg);
            /*textView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onTextClick(view, getAdapterPosition());
                    }
                }
            });

            imageView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onImageClick(view, getAdapterPosition());
                    }
                }
            });*/

        }
    }
}
