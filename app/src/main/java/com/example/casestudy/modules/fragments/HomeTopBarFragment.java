package com.example.casestudy.modules.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.casestudy.R;
import com.example.casestudy.modules.InquireActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeTopBarFragment extends Fragment {
    private View view;

    public HomeTopBarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_top_bar, container, false);

        Button inqBtn = view.findViewById(R.id.inqButton);
        inqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GoToInquiryActivity();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }


    public void GoToInquiryActivity() {
        Intent intent = new Intent(getActivity(), InquireActivity.class);
        startActivity(intent);
    }

}
