package com.example.casestudy.modules.objects;

import android.net.Uri;

public class Category {
    private int image;

    public int getImage(){
        return image;
    }
    public void setImage(int drawableNumber) {this.image = drawableNumber; }


    private String categoryName;

    public String getCategoryName(){ return categoryName; }
    public void setCategoryName(String categoryName) {this.categoryName = categoryName; }

    private Boolean isSelected = false;

    public boolean checkIfSelected(){ return isSelected; }
    public void toggleSelected(){
        if(this.isSelected == true){
            this.isSelected = false;
        }
        else{
            this.isSelected = true;
        }
    }

}
