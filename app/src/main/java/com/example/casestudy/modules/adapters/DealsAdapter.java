package com.example.casestudy.modules.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.casestudy.R;
import com.example.casestudy.modules.objects.Category;
import com.example.casestudy.modules.objects.Deal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.ViewHolder>{
    private List<Deal> mData;


    public DealsAdapter( ArrayList<Deal> data) {
        this.mData = data;
    }

    public DealsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.deals_recyclerview_cell, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String dealTitle = mData.get(position).getDealTitle();
        String dealCompany = mData.get(position).getDealCompany();

        Picasso.get().load(mData.get(position).getImage()).fit().centerCrop().into(holder.imageView);

        holder.textView1.setText(dealTitle);
        holder.textView2.setText(dealCompany);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView1;
        TextView textView2;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.dealTitleTextView);
            textView2 = itemView.findViewById(R.id.dealCompanyTextView);
            imageView = itemView.findViewById(R.id.dealImg);
            /*textView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onTextClick(view, getAdapterPosition());
                    }
                }
            });

            imageView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onImageClick(view, getAdapterPosition());
                    }
                }
            });*/

        }
    }

}
