package com.example.casestudy.modules.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.casestudy.R;
import com.example.casestudy.modules.models.IItemClickListener;
import com.example.casestudy.modules.objects.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SearchCategoryAdapter extends RecyclerView.Adapter<SearchCategoryAdapter.ViewHolder>{
    private List<Category> mData;
    private ArrayList<String> selectedCategories;

    private IItemClickListener mClickListener;

    public SearchCategoryAdapter( ArrayList<Category> data) {
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_category_recyclerview_cell, viewGroup, false);
        selectedCategories = new ArrayList<String>();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String categoryName = mData.get(position).getCategoryName();
        Picasso.get().load(mData.get(position).getImage()).into(holder.imageView);

        holder.textView.setText(categoryName);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<String> toggleCategory(View itemView, int position){
        Log.e("sampleTag", "click card");

        Category category = mData.get(position);
        category.toggleSelected();

        if(category.getImage() == R.drawable.ic_car_white_48 && category.checkIfSelected()){
            category.setImage(R.drawable.ic_car);
        }
        else if(category.getImage() == R.drawable.ic_car && !category.checkIfSelected()){
            category.setImage(R.drawable.ic_car_white_48);
        }
        else if(category.getImage() == R.drawable.ic_food_white_48 && category.checkIfSelected()){
            category.setImage(R.drawable.ic_food);
        }
        else if(category.getImage() == R.drawable.ic_food && !category.checkIfSelected()){
            category.setImage(R.drawable.ic_food_white_48);
        }
        else if(category.getImage() == R.drawable.ic_beach_white_48 && category.checkIfSelected()){
            category.setImage(R.drawable.ic_beach);
        }
        else if(category.getImage() == R.drawable.ic_beach && !category.checkIfSelected()){
            category.setImage(R.drawable.ic_beach_white_48);
        }
        else if(category.getImage() == R.drawable.ic_calendar_white_48 && category.checkIfSelected()){
            category.setImage(R.drawable.ic_calendar);
        }
        else if(category.getImage() == R.drawable.ic_calendar && !category.checkIfSelected()){
            category.setImage(R.drawable.ic_calendar_white_48);
        }
        else if(category.getImage() == R.drawable.ic_shop_white_48 && category.checkIfSelected()){
            category.setImage(R.drawable.ic_shop);
        }
        else if(category.getImage() == R.drawable.ic_shop && !category.checkIfSelected()){
            category.setImage(R.drawable.ic_shop_white_48);
        }

        if (category.checkIfSelected()){
            itemView.setBackgroundResource(R.drawable.white_circle_icon_white_bg);
        }
        else{
            itemView.setBackgroundResource(R.drawable.white_circle_icon_no_bg);
        }

        if(category.checkIfSelected()){
            selectedCategories.add(category.getCategoryName());
        }
        else{
            selectedCategories.remove(category.getCategoryName());
        }

        Picasso.get().load(category.getImage()).into((ImageView) itemView);
        return selectedCategories;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.categoryText);
            imageView = itemView.findViewById(R.id.categoryImg);
            imageView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onImageClick(view, getAdapterPosition());
                    }
                }
            });
        }
    }

    // allows clicks events to be caught
    public void setClickListener(IItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

}
