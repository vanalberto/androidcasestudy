package com.example.casestudy.modules;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.casestudy.R;
import com.example.casestudy.modules.objects.Inquiry;
import com.example.casestudy.modules.objects.RadioGroupTableLayout;
import com.example.casestudy.modules.presenters.IInquiryPresenter;
import com.example.casestudy.modules.presenters.InquiryPresenter;
import com.example.casestudy.modules.views.IInquiryView;

public class InquireActivity extends AppCompatActivity implements IInquiryView {
    private IInquiryPresenter iInquiryPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquire);

        init();
    }

    public void GoToMainActivity(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onSendInquiry(View v){
        TextView invalidNameTextView = findViewById(R.id.invalid_name_textView);
        TextView invalidEmailTextView = findViewById(R.id.invalid_email_textView);

        invalidNameTextView.setVisibility(View.GONE);
        invalidEmailTextView.setVisibility(View.GONE);

        iInquiryPresenter.onSendInquiry(setInquiry());
    }

    @Override
    public void onSuccessfulInquiry(String message) {

    }

    @Override
    public void onFailedInquiry(String errorType) {

        if(errorType == "invalidName"){
            TextView textView = findViewById(R.id.invalid_name_textView);
            textView.setVisibility(View.VISIBLE);
        }
        else if(errorType == "invalidEmail"){
            TextView textView = findViewById(R.id.invalid_email_textView);
            textView.setVisibility(View.VISIBLE);
        }

    }

    public void init(){ iInquiryPresenter = new InquiryPresenter(this); }

    public Inquiry setInquiry(){
        Inquiry inquiry = new Inquiry();

        EditText editTextFullName = findViewById(R.id.inq_full_name_txt);
        EditText editTextEmailAdd = findViewById(R.id.inq_email_add_txt);
        RadioGroupTableLayout radioGroupTableLayout = findViewById(R.id.inq_type_radio_grp);
        RadioButton checkedRadioButtonId = radioGroupTableLayout.getCheckedRadioButtonId();
        EditText editTextMessage = findViewById(R.id.message_box_editTxt);

        inquiry.setFullName(editTextFullName.getText().toString());
        inquiry.setEmailAddress(editTextEmailAdd.getText().toString());

        if(checkedRadioButtonId != null){
            inquiry.setInquiryType(checkedRadioButtonId.getText().toString());
        }
        else{
            inquiry.setInquiryType("");
        }

        inquiry.setMessage(editTextMessage.getText().toString());

        return inquiry;
    }

}
