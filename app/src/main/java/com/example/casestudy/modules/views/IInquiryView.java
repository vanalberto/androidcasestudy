package com.example.casestudy.modules.views;

public interface IInquiryView {
    void onSuccessfulInquiry (String message);
    void onFailedInquiry(String errorType);
}
